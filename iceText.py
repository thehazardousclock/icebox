# Python code to illustrate Sending mail with attachments 
# from your Gmail account 

# libraries to be imported 
import smtplib 
import csv

from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 


#Send a text message a function because it will make it easier to include it iin other code.
def sendTextMsg():
	
	#change this to the location it will be used on
    with open("/home/pi/finalProj/textData.csv") as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
          
		  #moved all the phone #'s and passwords out to a csv so it is a little more secure.
            toaddr   = row[0]
            fromaddr = row[1]
            mailPw   = row[2]
            phoneL   = row[3] #Lance #
            phoneA   = row[4] #Adam #
            phoneD   = row[5] #Don #



#Everything below is used as part of MIME to send out email/text
#Most phone service providers have a phone#@email address that you can send an email to and it will be received as text.
    # instance of MIMEMultipart 
    msg = MIMEMultipart() 

    # storing the senders email address 
    msg['From'] = fromaddr 

    # storing the receivers email address 
    msg['To'] = toaddr 

    # storing the subject 
    msg['Subject'] = ""

    # string to store the body of the mail 
    body = "Attention! Someone has attempted to access your food!!"

    # attach the body with the msg instance 
    msg.attach(MIMEText(body, 'plain')) 


    # creates SMTP session 
    s = smtplib.SMTP('smtp.gmail.com', 587) 

    # start TLS for security 
    s.starttls() 

    # Authentication 
    s.login(fromaddr, mailPw) 

    # Converts the Multipart msg into a string 
    text = msg.as_string() 

    # sending the mail  add all text users here
 
    s.sendmail(fromaddr, phoneL, text) 


    # terminating the session 
    s.quit() 
 
#can use this function for standalone testing. 
#sendTextMsg()
