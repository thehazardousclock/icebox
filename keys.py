import time
import RPi.GPIO as GPIO
from keypad import keypad
 
GPIO.setwarnings(False)

def keys():

    kp = keypad(columnCount = 4)
 
    ###### 4 Digit wait ######
    seq = []
    for i in range(4):
        digit = None
        while digit == None:
            digit = kp.getKey()
        seq.append(digit) #creates our list of pins pressed from the keypad.
        print('*', end = '',flush = True) #this allows us to print **** to the console each time a number is pressed.
	#print("You have entered: " + str(digit))
        time.sleep(0.4)
    print("\n") 
    
    #***this was used to check a sequence as a standalone to prep for our passwords.
    # Check digit code
   # print(seq)
   # if seq == [1, 2, 3, '#']:
   #     print ("Code accepted")
   # else:
#	print ("FAILED CODE!")


    return seq
