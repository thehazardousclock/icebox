import lcdDisp
import campi
import keys
import time
import iceMail
import iceText

seq = []




#lcdDisp.lcd_string(message, 0x80)  Line 1
#lcdDisp.lcd_string(message, 0xc0)  Line 2
#lcdDisp.lcd_string(message, 0x94)  Line 3
#lcdDisp.lcd_string(message, 0xd4)  Line 4

#Used primarily for testing


#this grabs the combination of 4 keys pressed and stores this list into our sequence
seq = keys.keys()



#this can be adjusted to account for editing passwords or adding more passwords.


if seq == [1,1,1,1]:
    lcdDisp.lcd_string("Correct Password", 0x80)
    iceMail.sendTextMsg() #Lets us know if anyone enters our box.
else:
    lcdDisp.lcd_string("Incorrect Pass", 0x80)
    campi.thiefDetect() #takes a picture of someone entering incorrect password.
    iceMail.sendEmail() #sends email with picture of the intruder
    iceText.sendTextMsg() #sends a text message letting us know someone attempted to enter our box.
    campi.stampMove() #time stamps our picture and moves it to our intruder folder.
time.sleep(3) 
lcdDisp.lcd_init() #after 3 seconds the LCD goes blank/clears
