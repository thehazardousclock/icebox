#Read char for getting a char/testing on windows
#Make sure to pip install readchar to read char input
#import readchar
import os
import time
import keypad
import lcdDisp
import iceMail
import iceText
import campi
import FPRunTest as fScanner

import RPi.GPIO as io
#Main Function

#Constants:
SEL_UNLOCK = 0
SEL_LOCK = 1
SEL_SETTINGS = 2


#State of events.
# Check if the password.txt file exists. If it exists then a password is present and normal operation can occur
# If it doesn't exist, then promp the user to enter a password.


#A basic state machine
#Only need two states. Unlock and Lock. True False
CURR_STATE = False
LOCKED = False
UNLOCKED = True

kp = keypad.keypad()

def main():
    selection = 0
    #initilize scanner
    fScanner.initScanner()
    io.setup(18, io.OUT)
    
    #Check for a password.
    #If there is no password, then initilize one. 
    if not PasswordPresent():
        InitPassword()
        initFinger()
    
    while True:
        #Prevent from running away from allowed values
        if selection > SEL_SETTINGS:
            selection = SEL_SETTINGS
        elif selection < SEL_UNLOCK:
            selection = SEL_UNLOCK
        
        #Refresh GUI
        GUI(selection)
        
        #Get input
        controlIn = GetInput()
        if controlIn == b'8':
            selection = selection + 1
        elif controlIn == b'2':
            selection = selection - 1
        elif controlIn == b'9':
            #This is where exeuction of code is terminated. Replace b'e' with what ever 0 on the keypad is.
            #Unless we can use a button as a "Enter" key.
            execution(selection)
        #else:
            #exit()

def toggleLock():
    global CURR_STATE
    global LOCKED
    global UNLOCKED
    
    if CURR_STATE == LOCKED:
        io.output(18, 1)
    else:
        io.output(18, 0)
    

def initFinger():
    clearScreen()
    lineOne("Place finger on")
    lineTwo("Scanner while the")
    lineThree("Light is on")
    fScanner.saveFinger()
    
    
def clearScreen():
    lcdDisp.lcd_init()

def lineOne( msg ):
    lcdDisp.lcd_string(msg, 0x80)

def lineTwo( msg ):
    lcdDisp.lcd_string(msg, 0xC0)
    
def lineThree( msg ):
    lcdDisp.lcd_string(msg, 0x94)

#Our code for unlocking, locking and settings goes here.
def unlock():
    passInput = b''
    for i in range(0,4):
        #Clear screen. Prompt for password char input
        clearScreen()
        lineOne("Enter Pass " + str(passInput))
        passInput = passInput + GetInput()
    clearScreen()
    lineOne("Enter Pass " + str(passInput))
    passwordString = ""
    try:
        pFile = open("password.txt", "r")
        passwordString = pFile.read(4)
        pFile.close()
    except IOError:
        print("ERR: Password File does Not Exist")
        return
    if passwordString == passInput.decode("utf-8"):
        #Unlock Confirmation Goes Here. Activate Solenoid.
        clearScreen()
        lineOne("Pass Correct")
        
        lineTwo("Scan Finger")
        
        
        check = fScanner.checkFinger()
        if(check == True):
            clearScreen()
            lineOne("Finger Found.")
            lineTwo("Unlocking...")
            global CURR_STATE
            global LOCKED
            global UNLOCKED
            if CURR_STATE == LOCKED:
                CURR_STATE = UNLOCKED
                toggleLock()
            time.sleep(2)
            try:
                iceText.sendTextMsg()
            except:
                print("Somthing went wrong")
            return
        
        clearScreen()
        lineOne("Too many tries!")
        lineTwo("Returning...")
        time.sleep(2)
        
        
        try:
            campi.thiefDetect() #takes a picture of someone entering incorrect password.
            iceMail.sendEmail() #sends email with picture of the intruder
            iceText.sendTextMsg() #sends a text message letting us know someone attempted to enter our box.
            campi.stampMove()
        except:
            print("Somthing went wrong")
        time.sleep(2)
    else:
        #Wrong Password. Do nothing.
        clearScreen()
        lineOne("Incorrect Pass")
        try:
            campi.thiefDetect() #takes a picture of someone entering incorrect password.
            iceMail.sendEmail() #sends email with picture of the intruder
            iceText.sendTextMsg() #sends a text message letting us know someone attempted to enter our box.
            campi.stampMove()
        except:
            print("Somthing went wrong")
        time.sleep(2)


def lock():
    global CURR_STATE
    global LOCKED
    global UNLOCKED
    if CURR_STATE == UNLOCKED:
        CURR_STATE = LOCKED
        toggleLock()
    clearScreen()
    lineOne("Locking...")
    time.sleep(1)

def SettingsGUI():
    while True:
        clearScreen()
        lineOne("Reset everything?")
        lineTwo("8 - YES")
        lineThree("2 - NO")
        
        checkInput = GetInput()
        
        if( checkInput == 8 ):
            properlyReset = resetEverything()
            
            if properlyReset == False:
                break
        elif checkInput == 2:
            #Return to main GUI by breaking from loop
            break

def resetEverything():
    passInput = b''
    for i in range(0,8):
        #Clear screen. Prompt for password char input
        clearScreen()
        lineOne("Enter Master Pass:")
        lineTwo(str(passInput))
        passInput = passInput + GetInput()
    clearScreen()
    lineOne("Enter Pass " + str(passInput))
    passwordString = ""
    try:
        pFile = open("masterPass.txt", "r")
        passwordString = pFile.read(8)
        pFile.close()
    except IOError:
        print("ERR: Password File does Not Exist")
        return
    if passwordString == passInput.decode("utf-8"):
        #Unlock Confirmation Goes Here. Activate Solenoid.
        clearScreen()
        lineOne("Pass Correct")
        lineTwo("Resetting...")
        os.remove("password.txt")
        time.sleep(2)
        main()
    
def settings():
    lineOne("")

#Checks if a password exists. If it does, return true, otherwise return false.
def PasswordPresent():
    if os.path.isfile("password.txt"):
        return True
    else:
        return False

#Executes function based on selection.
def execution( selection ):

    if( selection == SEL_UNLOCK ):
        unlock()
    elif( selection == SEL_LOCK ):
        lock()
    elif( selection == SEL_SETTINGS ):
        settings()
    else:
        print("Wtf? How are you even here?")


#Replace this with your keypad code for input. 
#Use 2 and 8 for up and down arrows, 0 for enter.
def GetInput():
    getVal = kp.getKey()
    
    while getVal == None:
        getVal = kp.getKey()
    while kp.getKey() != None:
        time.sleep(0.01)
    return (getVal+48).to_bytes(1, byteorder='big')

#Main GUI
#Print selections. On input, clear the screen and reprint
def GUI(sel):

    #Clear screen
    clearScreen()
    #Print selection
    if sel == 0:
        lineOne( "> Unlock")
        lineTwo( "Lock" )
        lineThree("Settings")
        
        print("> Unlock\n Lock\n Settings\r\r\r", end='')
    elif sel == 1:
        lineOne( "Unlock")
        lineTwo( "> Lock" )
        lineThree("Settings")
        print(" Unlock\n> Lock\n Settings\r\r\r", end='')
    else:
        lineOne( "Unlock")
        lineTwo( "Lock" )
        lineThree("> Settings")
        print(" Unlock\n Lock\n> Settings\r\r\r", end='')

#Initilize the password.
#Prompt the user twice for input.
#Compare the two. If the second doesn't match the first, reprompt until input is correct. 
#Pause 1 second between each input.
def InitPassword():
    firstPass = b''
    secondPass = b''
    while True:
        for i in range(0,4):
            #Clear screen. Prompt for password char input
            clearScreen()
            lineOne("Enter Pass: ")
            lineTwo(str(firstPass))
            firstPass = firstPass + GetInput()
        clearScreen()
        lineOne("Enter Pass: ")
        lineTwo(str(firstPass))
        time.sleep(1)
        for i in range(0, 4):
            #Clear Screen. Print to console.
            clearScreen()
            lineOne("Re-Enter Pass:")
            lineTwo(str(secondPass))
            secondPass = secondPass + GetInput()
        clearScreen()
        lineOne("Re-Enter Pass")
        lineTwo(str(secondPass))
        
        if( firstPass == secondPass ):
            #Save the password then exit
            pFile = open("password.txt", "w")
            pFile.write(firstPass.decode("utf-8"))
            pFile.close()
            break
        #Passwords do not match. Loop again.
        lineOne("Passwords do not match")
        firstPass = b''
        secondPass = b''
        time.sleep(1)

    



if __name__ == "__main__":
    main()