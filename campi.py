from picamera import PiCamera
from time import sleep
import datetime
import time
import os

camera = PiCamera()

#preview used if you want to see a visual preview of the picture that will be taken.
#used this for testing, but not needed for the actual program
#camera.start_preview()
#sleep(2)
#camera.stop_preview()


#created a date for datetime so after we email out the picture we can store it in a folder by date and time
#this will help to keep track of when the photo was taken
date = datetime.datetime.now().strftime("%m-%d-%y %H:%M:%S")



#this function takes a picture and stores it as image.jpg on the pi desktop
def thiefDetect():
    camera.capture("/home/pi/Desktop/image.jpg")


#moves the file to our intruders folder and timestamps it
def stampMove():
    
    os.rename("/home/pi/Desktop/image.jpg","/home/pi/finalProj/intruders/" +date + ".jpg")
    
