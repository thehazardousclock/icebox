#Thumb Constants

#===SERIAL SETTINGS===#
import serial
SER_PORT = "/dev/ttyAMA0"
SER_RATE = 57600
SER_PARITY = serial.PARITY_NONE
SER_STOPBITS = serial.STOPBITS_ONE
SER_BYTESIZE = serial.EIGHTBITS
SER_TIMEOUT  = 5.0
SER_XONXOFF  = 0
SER_RTSCTS   = 0



PASSWORD = b'\x00\x00\x00\x00'
ADDRESS = b'\xFF\xFF\xFF\xFF'
HEADER = b'\xEF\x01'

PACKET_COMMAND      = b'\x01'
PACKET_DATA         = b'\x02'
PACKET_ACK          = b'\x07'
PACKET_ENDOFDATA    = b'\x08'

CHECK_SUM_LENGTH    = 2

ACK_COMMAND_EXECUTION_COMPLETE      = b'\x00'
ACK_ERROR_RECV_PCK                  = b'\x01'
ACK_NO_FINGER_DETECTED              = b'\x02'
ACK_FAILED_TO_ENROLL_FINGER         = b'\x03'
ACK_FAILEDTGCHR_FILE_DISORDERLY     = b'\x06'       #Failed to generate character file to due over-disorderly fingerprint img
ACK_FAILEDTGCHR_FILE_WEAKNESS       = b'\x07'       #Failed to generate due to point weakness
ACK_FINGER_NOT_MATCH                = b'\x08'
ACK_FINGER_NOT_FOUND                = b'\x09'
ACK_FAILED_COMBINECHR_FILE          = b'\x0A'       #Failed to combine chracter Files
ACK_ADDR_PAGEID_OVERFLOW            = b'\x0B'
ACK_ERR_READ_TEMPLATE               = b'\x0C'
ACK_ERR_UPLOADING_TEMPLATE          = b'\x0D'
ACK_MODULE_CNT_RCV_PCKS             = b'\x0E'
ACK_ERR_UPLOADING_IMG               = b'\x0F'
ACK_FAILED_DELETE_TEMPLATE          = b'\x10'
ACK_FAIL_CLEAR_FINGR_LIB            = b'\x11'
ACK_FAIL_GENERATE_IMG               = b'\x15'
ACK_ERR_WRITE_FLASH                 = b'\x18'
ACK_INVALID_REGISTER                = b'\x1A'


#DATA PACKETS
#---VERIFY PASSWORD---#
DATA_VERIFY_PASSWORD = b'\x13\x00\x00\x00\x00'
SIZE_VERIFY_PASSWORD = b'\x00\x07'
CODE_VERIFY_PASSWORD = (0).to_bytes(4,byteorder='big')
CSUM_VERIFY_PASSWORD = b'\x00\x1B'


#---GetImg---#
DATA_GETIMG             = b'\x01'
SIZE_GETIMG             = b'\x00\x03'
CSUM_GETIMG             = b'\x00\x05'
CODE_GETIMG             = b'\x01'


#---Open Fingerprint Lighting---# Trurn on the LED???
CODE_LIGHTING_OPEN           = b'\x50'
SIZE_LIGHTING_OPEN           = b'\x00\x03'
CSUM_LIGHTING_OPEN           = b'\x00\x54'


#--CLOSE Fingerprint Lighting---# #Turn off led?
CODE_LIGHTING_CLOSE          = b'\x51'
SIZE_LIGHTING_CLOSE          = b'\x00\x03'
CSUM_LIGHTING_CLOSE          = b'\x00\x55'


#--- Get Image Free Lighting ---#
#Get image with free lighting?
CODE_GETIMG_FREE             = b'\x52'
SIZE_GETIMG_FREE             = b'\x00\x03'
CSUM_GETIMG_FREE             = b'\x00\x56'


#--- ECHO ---#
#Used to check if the scanner is in normal working order
CODE_ECHO                   = b'\x53'
SIZE_ECHO                   = b'\x00\x03'
CSUM_ECHO                   = b'\x00\x57'

#--- IMG2TZ ---#
#Take the image from the image buffer and turn it into a character file
CODE_IMG2TZ                 = b'\x02'
SIZE_IMG2TZ                 = b'\x00\x04'
CSUM_IMG2TZ_CHR1             = b'\x00\x08'
CSUM_IMG2TZ_CHR2             = b'\x00\x09'
BUFFER_IMG2TZ_CHR1          = b'\x01'
BUFFER_IMG2TZ_CHR2          = b'\x02'
DATA_IMG2TZ_CHR1            = SIZE_IMG2TZ + CODE_IMG2TZ + BUFFER_IMG2TZ_CHR1 + CSUM_IMG2TZ_CHR1
DATA_IMG2TZ_CHR2            = SIZE_IMG2TZ + CODE_IMG2TZ + BUFFER_IMG2TZ_CHR2 + CSUM_IMG2TZ_CHR2


#-- Upload Character to Computer ---#
CODE_UPCHAR                 = b'\x08'
SIZE_UPCHAR                 = b'\x00\x04'
BUFFERONE_UPCHAR            = b'\x01'
BUFFERTWO_UPCHAR            = b'\x02'
CSUM_UPCHAR_ONE             = b'\x0E'
CSUM_UPCHAR_TWO             = b'\x0F'
DATA_UPCHAR_CHRONE          = SIZE_UPCHAR + CODE_UPCHAR + BUFFERONE_UPCHAR + CSUM_UPCHAR_ONE
DATA_UPCHAR_CHRONE          = SIZE_UPCHAR + CODE_UPCHAR + BUFFERTWO_UPCHAR + CSUM_UPCHAR_TWO
