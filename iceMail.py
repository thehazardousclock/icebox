    

# libraries to be imported 
import os
import smtplib 
import csv
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 


def sendEmail():
    
    #checking to see if there is an image on the desktop. 
    #If not, I had a default jpeg emailed instead to let us know the picture was not taken of the intruder.
    
    if(os.path.exists('/home/pi/Desktop/image.jpg')):
            attachPath = "/home/pi/Desktop/image.jpg"
            filename = "image.png"
    else:
        attachPath = "/home/pi/Desktop/oops.jpeg"
        filename = "oops.jpeg"
        
    #adjust this path as needed.
    with open("/home/pi/finalProj/emailData.csv") as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
          
          #moved all the phone #'s and passwords out to a csv so it is a little more secure.
            fromaddr     = row[0]
            toaddr       = row[1]
            emailPass    = row[2]
            
#everything below is the somewhat the default setup using MIME to send our emails.            
            
    # instance of MIMEMultipart 
    msg = MIMEMultipart() 

    # storing the senders email address 
    msg['From'] = fromaddr 

    # storing the receivers email address 
    msg['To'] = toaddr 

    # storing the subject 
    msg['Subject'] = "Subject of the Mail"

    # string to store the body of the mail 
    body = "Body_of_the_mail"

    # attach the body with the msg instance 
    msg.attach(MIMEText(body, 'plain')) 

    # open the file to be sent 
    #filename = "oops.jpeg"
    attachment = open(attachPath, "rb") 

    # instance of MIMEBase and named as p 
    p = MIMEBase('application', 'octet-stream') 

    # To change the payload into encoded form 
    p.set_payload((attachment).read()) 

    # encode into base64 
    encoders.encode_base64(p) 

    p.add_header('Content-Disposition', "attachment; filename= %s" % filename) 

    # attach the instance 'p' to instance 'msg' 
    msg.attach(p) 

    # creates SMTP session 
    s = smtplib.SMTP('smtp.gmail.com', 587) 

    # start TLS for security 
    s.starttls() 

    # Authentication 
    s.login(fromaddr, emailPass ) 

    # Converts the Multipart msg into a string 
    text = msg.as_string() 

    # sending the mail 
    s.sendmail(fromaddr, toaddr, text) 

    # terminating the session 
    s.quit() 
    
    #can use the fuction below if testing this alone.
#sendEmail()
